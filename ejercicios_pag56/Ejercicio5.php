<!DOCTYPE html>
<html>
<body>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Número 1: <input type="number" name="numero1"><br>
  Número 2: <input type="number" name="numero2"><br>
  Número 3: <input type="number" name="numero3"><br>
  Número 4: <input type="number" name="numero4"><br>
  <input type="submit" value="Calcular">
</form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $numero1 = $_POST["numero1"];
  $numero2 = $_POST["numero2"];
  $numero3 = $_POST["numero3"];
  $numero4 = $_POST["numero4"];
  $suma = $numero1 + $numero2 + $numero3 + $numero4;
  $porcentaje1 = ($numero1 / $suma) * 100;
  $porcentaje2 = ($numero2 / $suma) * 100;
  $porcentaje3 = ($numero3 / $suma) * 100;
  $porcentaje4 = ($numero4 / $suma) * 100;
  echo "Porcentaje del número 1: " . $porcentaje1 . "%<br>";
  echo "Porcentaje del número 2: " . $porcentaje2 . "%<br>";
  echo "Porcentaje del número 3: " . $porcentaje3 . "%<br>";
  echo "Porcentaje del número 4: " . $porcentaje4 . "%<br>";
}
?>
</body>
</html>
