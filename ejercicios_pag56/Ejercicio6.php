<!DOCTYPE html>
<html>
<body>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Lado del cuadrado: <input type="number" name="lado"><br>
  <input type="submit" value="Calcular">
</form>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $lado = $_POST["lado"];
  $area = $lado * $lado;
  $perimetro = 4 * $lado;
  echo "El área del cuadrado es: " . $area . "<br>";
  echo "El perímetro del cuadrado es: " . $perimetro . "<br>";
}
?>
</body>
</html>
