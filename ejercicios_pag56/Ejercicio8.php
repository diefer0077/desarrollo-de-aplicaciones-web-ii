<!DOCTYPE html>
<html>
<body>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Grados Fahrenheit: <input type="number" name="fahrenheit"><br>
  <input type="submit" value="Calcular">
</form>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $fahrenheit = $_POST["fahrenheit"];
  $celsius = ($fahrenheit - 32) * 5/9;
  $kelvin = ($fahrenheit + 459.67) * 5/9;
  echo $fahrenheit . " grados Fahrenheit equivalen a " . $celsius . " grados Celsius y " . $kelvin . " grados Kelvin";
}
?>
</body>
</html>
