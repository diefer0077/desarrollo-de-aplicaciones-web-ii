<!DOCTYPE html>
<html>
<body>

<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Primer número: <input type="number" name="numero1"><br>
  Segundo número: <input type="number" name="numero2"><br>
  <input type="submit" value="Enviar">
</form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $a = $_POST["numero1"];
  $b = $_POST["numero2"];
  $cont = 0;
  for ($i = $a; $i <= $b; $i++) {
      echo $i . " ";
      $cont++;
  }
  echo "<br>Hay un total de " . $cont . " números enteros entre " . $a . " y " . $b;
}
?>

</body>
</html>
