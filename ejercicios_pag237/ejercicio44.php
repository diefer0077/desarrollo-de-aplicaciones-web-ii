<?php
function fibonacci($n) {
  $fibonacci = array(0, 1);
  while (end($fibonacci) < $n) {
    $fibonacci[] = end($fibonacci) + prev($fibonacci);
  }
  array_pop($fibonacci);
  return $fibonacci;
}

if (isset($_POST['submit'])) {
  $n = $_POST['n'];
  $fibonacci = fibonacci($n);
  $sum = array_sum($fibonacci);
  $count = count($fibonacci);
  echo "La serie de Fibonacci menor a $n es: " . implode(", ", $fibonacci) . "<br>";
  echo "La suma de los números de la serie es: $sum<br>";
  echo "La cantidad de números en la serie es: $count<br>";
}
?>

<form method="post">
  <label for="n">Ingrese un número:</label>
  <input type="number" name="n" id="n">
  <input type="submit" name="submit" value="Calcular">
</form>