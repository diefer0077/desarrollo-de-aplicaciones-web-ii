<html>
  <head>
    <title>Suma de filas de una matriz</title>
  </head>
  <body>
    <form method="post">
      <label>Ingrese los números:</label><br>
      <input type="number" name="matriz[0][0]">
      <input type="number" name="matriz[0][1]"><br>
      <input type="number" name="matriz[1][0]">
      <input type="number" name="matriz[1][1]"><br>
      <input type="number" name="matriz[2][0]">
      <input type="number" name="matriz[2][1]"><br><br>
      <input type="submit" value="Calcular suma">
    </form>
    
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $matriz = $_POST["matriz"];
      $suma_filas = array();
      
      for ($i = 0; $i < count($matriz); $i++) {
        $suma = 0;
        for ($j = 0; $j < count($matriz[$i]); $j++) {
          $suma += $matriz[$i][$j];
        }
        $suma_filas[] = $suma;
      }
      
      echo "<br>La suma de cada fila es:<br>";
      foreach ($suma_filas as $suma) {
        echo $suma . "<br>";
      }
    }
    ?>
  </body>
</html>
