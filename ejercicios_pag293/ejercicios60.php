<!DOCTYPE html>
<html>
<head>
    <title>obtener números mayores de cada columna</title>
</head>
<body>
    <h2>Ingreso valores en la matriz:</h2>
    <form method="post">
        <table>
            <?php
            $matriz = array();
            for ($i = 0; $i < 4; $i++) {
                echo "<tr>";
                for ($j = 0; $j < 3; $j++) {
                    echo "<td><input type='number' name='matrices[$i][$j]' required></td>";
                }
                echo "</tr>";
            }
            ?>
        </table>
        <br>
        <label for="k">Ingrese el valor de K:</label>
        <input type="numero" name="k" required>
        <br><br>
        <input type="submit" value="Calcular">
    </form>
    <br><br>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $k = intval($_POST["k"]);
        $matriz = $_POST['matrices'];
        $mayores = array();
        for ($j = 0; $j < count($matriz[0]); $j++) {
            $mayor = $matriz[0][$j];
            for ($i = 1; $i < count($matriz); $i++) {
                if ($matriz[$i][$j] > $mayor) {
                    $mayor = $matriz[$i][$j];
                }
            }
            $mayores[$j] = $mayor * $k;
        }
        
        echo "Números mayores de cada columna multiplicados por K:<br>";
        for ($j = 0; $j < count($mayores); $j++) {
            echo "Columna " . ($j+1) . ": " . $mayores[$j] . "<br>";
        }
        $suma = 0;
        foreach ($matriz as $fila) {
            foreach ($fila as $valor) {
                $suma += $valor;
            }
        }
        echo "<br>Suma de los números de las matrices: " . $suma;
    }
    ?>
</body>
</html>
