<!DOCTYPE html>
<html>

<head>
    <title>Ordenar números</title>
</head>

<body>
    <h1>Ordenar números</h1>
    <form method="post" action="">
        <label for="num1">Número 1:</label>
        <input type="number" id="num1" name="numeros[]" required>
        <br><br>
        <label for="num2">Número 2:</label>
        <input type="number" id="num2" name="numeros[]" required>
        <br><br>
        <label for="num3">Número 3:</label>
        <input type="number" id="num3" name="numeros[]" required>
        <br><br>
        <label for="num4">Número 4:</label>
        <input type="number" id="num4" name="numeros[]" required>
        <br><br>
        <label for="num5">Número 5:</label>
        <input type="number" id="num5" name="numeros[]" required>
        <br><br>
        <label for="forma">Forma:</label>
        <select id="forma" name="forma">
            <option value="ascendente">Ascendente</option>
            <option value="descendente">Descendente</option>
        </select>
        <br><br>
        <input type="submit" value="Ordenar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $numeros = $_POST["numeros"];
        $forma = $_POST["forma"];

        if ($forma == "ascendente") {
            sort($numeros);
        } else {
            rsort($numeros);
        }

        echo "<p>Orden: " . implode(", ", $numeros) . "</p>";
    }
    ?>
</body>

</html>