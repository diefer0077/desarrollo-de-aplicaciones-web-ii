<!DOCTYPE html>
<html>
<head>
	<title>Calcular número mayor y menor</title>
	<style>
		form {
			display: flex;
			flex-direction: column;
			align-items: center;
		}

		input[type="submit"] {
			margin-top: 10px;
		}

		.resultado {
			margin-top: 10px;
			display: none;
			text-align: center;
		}

		.mostrar {
			display: block;
		}
	</style>
</head>
<body>

<div class="contenedor">
	<form method="post">
	    <label for="num1">Número 1:</label>
	    <input type="number" name="num1"><br>

	    <label for="num2">Número 2:</label>
	    <input type="number" name="num2"><br>

	    <label for="num3">Número 3:</label>
	    <input type="number" name="num3"><br>

	    <label for="num4">Número 4:</label>
	    <input type="number" name="num4"><br>

	    <input type="submit" name="submit" value="Calcular">
	</form>

	<div class="resultado" id="resultado">
		<p>El número mayor es: <span id="mayor"></span></p>
		<p>El número menor es: <span id="menor"></span></p>
	</div>
</div>

<script>
	const form = document.querySelector('form');
	const resultado = document.getElementById('resultado');
	const mayor = document.getElementById('mayor');
	const menor = document.getElementById('menor');

	form.addEventListener('submit', (e) => {
		e.preventDefault();
		const num1 = parseFloat(form.num1.value);
		const num2 = parseFloat(form.num2.value);
		const num3 = parseFloat(form.num3.value);
		const num4 = parseFloat(form.num4.value);

		const numeros = [num1, num2, num3, num4];

		const numMayor = Math.max(...numeros);
		const numMenor = Math.min(...numeros);

		mayor.textContent = numMayor;
		menor.textContent = numMenor;

		resultado.classList.add('mostrar');
	});
</script>

</body>
</html>
