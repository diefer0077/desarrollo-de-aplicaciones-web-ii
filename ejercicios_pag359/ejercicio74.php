<!DOCTYPE html>
<html>
<head>
	<title>Suma de dígitos pares e impares</title>
</head>
<body>
	<form method="post" action="">
		<label>Introduce un número:</label>
		<input type="number" name="numero">
		<input type="submit" value="Calcular">
	</form>
	<?php
		if ($_POST) {
			$numero = $_POST['numero'];
			$suma_pares = 0;
			$suma_impares = 0;

			while ($numero > 0) {
				$digito = $numero % 10;
				if ($digito % 2 == 0) {
					$suma_pares += $digito;
				} else {
					$suma_impares += $digito;
				}
				$numero = intval($numero / 10);
			}

			echo "<p>La suma de los dígitos pares es: $suma_pares</p>";
			echo "<p>La suma de los dígitos impares es: $suma_impares</p>";
		}
	?>
</body>
</html>
