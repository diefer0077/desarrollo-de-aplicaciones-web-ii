<!DOCTYPE html>
<html>
<head>
  <title>Invertir número</title>
</head>
<body>
  <h1>Invertir número</h1>
  <form method="post">
    <label for="numero">Ingrese un número:</label>
    <input type="number" id="numero" name="numero">
    <button type="submit">Invertir</button>
  </form>
  <?php
    if (isset($_POST['numero'])) {
      $numero = $_POST['numero'];
      $numeroInvertido = invertirNumero($numero);
      echo "El número invertido es: " . $numeroInvertido;
    }
    
    function invertirNumero($numero) {
      $numeroInvertido = 0;
      while ($numero > 0) {
        $digito = $numero % 10;
        $numeroInvertido = $numeroInvertido * 10 + $digito;
        $numero = (int)($numero / 10);
      }
      return $numeroInvertido;
    }
  ?>
</body>
</html>
