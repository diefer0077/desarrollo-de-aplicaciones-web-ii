<!DOCTYPE html>
<html>
<head>
    <title>El promedio de las dos notas mayores</title>
</head>
<body>
    <h1>El promedio de las dos notas mayores</h1>
	<?php
		function calcularPromedio($n1, $n2, $n3) {
			$notas = [$n1, $n2, $n3];
			rsort($notas);
			return (floatval($notas[1]) + floatval($notas[2])) / 2;
		}
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$n1 = (float) $_POST['nota1'];
			$n2 = (float) $_POST['nota2'];
			$n3 = (float) $_POST['nota3'];
			$promedio = calcularPromedio($n1, $n2, $n3);
			
		}
	?>
	<form method="post">
		<label for="nota1">Introduce la primera nota:</label>
		<input type="number" name="nota1" id="nota1" required>
        <br>
		<label for="nota2">Introduce la segunda nota:</label>
		<input type="number" name="nota2" id="nota2" required>
        <br>
		<label for="nota3">Introduce la tercera nota:</label>
		<input type="number" name="nota3" id="nota3" required>
        <br>        <br>
		<button type="submit">Calcular promedio</button>
        <br>
	</form>
	<?php if (isset($promedio)) { ?>
		<p>El promedio de las dos notas mayores es: <?php echo number_format($promedio, 2, '.', '.'); ?></p>
	<?php } ?>
</body>
</html>


