<!DOCTYPE html>
<html>
<head>
  <title>Calculadora de área de un rectángulo</title>
</head>
<body>
  <h1>Calculadora de área de un rectángulo</h1>
  <form method="post">
    <label for="base">Base:</label>
    <input type="number" name="base" required><br>
    <label for="altura">Altura:</label>
    <input type="number" name="altura" required><br>
    <input type="submit" value="Calcular">
  </form>
  <?php
    function AreaRectangulo($base, $altura) {
      return $base * $altura;
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $base = $_POST["base"];
      $altura = $_POST["altura"];
      $area = AreaRectangulo($base, $altura);
      echo "<p>El área del rectángulo es: $area</p>";
    }
  ?>
</body>
</html>
