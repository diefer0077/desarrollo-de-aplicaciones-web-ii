<form method="POST">
  <label for="letra">Ingrese una letra:</label>
  <input type="text" name="letra" id="letra">
  <button type="submit">Verificar</button>
</form>

<?php
  if(isset($_POST["letra"])) {
    $letra = trim($_POST["letra"]);
    if (ctype_upper($letra)) {
      echo "La letra ingresada está en mayúscula";
    } elseif (ctype_lower($letra)) {
      echo "La letra ingresada está en minúscula";
    } else {
      echo "El valor ingresado no es una letra del alfabeto";
    }
  }
?>
